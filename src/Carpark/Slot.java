package Carpark;

public class Slot {
	Car car;
	boolean slotIsFree=true;
	int currentCarId;
	
	public void takeSlot(Car car) {
		this.car=car;
		slotIsFree=false;
	}
	public void checkout() {
		this.car=new Car();
		slotIsFree=true;
	}
}
