package Carpark;

public class Clock {
    int time;
    public int getTime() {
		return time;
	}
	public String printTime() {
    	return time/60+":"+String.format("%02d", time%60);
    }
    public void incrementTime() {
    	time++;
    }
}
