package Carpark;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Monitor {
	Carpark carpark;
	Clock clock;
	
	int lastKnownCarId=0;
	
	final int CAR_ARRIVES_ODDS = 65;
	
	volatile boolean vacant = true;
	
	public Monitor(Carpark carpark) {
		this.carpark = carpark;
		this.clock = carpark.clock;
		
		Thread timer = new Thread(new Timer(this));
		timer.start();
	}

	public void checkArrival() {
		int odds = Main.getRandom(0,100);
		if(odds<CAR_ARRIVES_ODDS) {
			Thread car = new Thread(new Car(this));
			car.start();	
		}
	}
	public void printCarpark() {
		carpark.slots.stream().
		forEach(m->{
			if(m.slotIsFree)
				System.out.print("[FREE]");
			else
				System.out.print("[CAR ID:"+m.car.carId+"]");
		});
		System.out.println();
	}
}
