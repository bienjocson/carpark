package Carpark;

import java.util.NoSuchElementException;

public class Car implements Runnable {
	int carId;
	int timeEntered;
	int lengthOfStay;
	
	Monitor monitor;

	final int TIME_UNIT = 5;
	final int MS_TO_SECS = 1000;
	final int MIN_LENGTH_OF_STAY = 1;
	final int MAX_LENGTH_OF_STAY = 3;

	boolean exit = false;
	public Car() {
		 
	}
	public Car(Monitor monitor) {
		this.monitor = monitor;
		this.carId = monitor.lastKnownCarId++;
		this.timeEntered = monitor.clock.time;
		this.lengthOfStay = Main.
				getRandom(MIN_LENGTH_OF_STAY, MAX_LENGTH_OF_STAY) * TIME_UNIT;
		try {
			System.out.println("Car " + carId + " has arrived: " + convertToTime(timeEntered) + " - " + convertToTime(timeEntered + lengthOfStay));
			monitor.carpark.carIsParked(this);
			monitor.printCarpark();
		} catch (NoSuchElementException e) {
			System.out.println("Carpark is full");
		}
	}
	public void sleep() {
		try { 
			Thread.sleep(MS_TO_SECS); 
		}	catch (InterruptedException e) { 
		}
	}
	public void checkout() {
		sleep();
		if (monitor.clock.getTime() >= (timeEntered + lengthOfStay)) {
			if(monitor.vacant) {
				monitor.vacant=false;
				System.out.println("Car ID: " + carId + " has entered checkout.");
				sleep();
				monitor.carpark.payParkingFee(this);
				monitor.carpark.carLeaves(this);
				monitor.vacant = true;
				synchronized(monitor) {
					monitor.notify();
				}
				exit=true;
			}
			else {
				synchronized(monitor) {
					try {
						System.out.println("Car ID: " + carId + " is waiting to checkout.");
						monitor.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			
		}
	}
	public String convertToTime(int time) {
		return time/60+":"+String.format("%02d", time%60);
	}
	@Override
	public void run() {
		while (!exit) {
			checkout();
		}
	}
}
