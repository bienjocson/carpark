package Carpark;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Carpark {
	Clock clock = new Clock();
	Monitor monitor;
	
	final int NUMBER_OF_SLOTS=10;
	ArrayList<Slot> slots = new ArrayList<Slot>(NUMBER_OF_SLOTS);
	public Carpark() {
		monitor = new Monitor(this);
		
		//INITIALIZE SLOTS
		for(int i=0;i<NUMBER_OF_SLOTS;i++)
			slots.add(new Slot());
	}
	public void carIsParked(Car car) {
		Slot slot=slots.stream().filter(m->m.slotIsFree).findFirst().get();
		slot.takeSlot(car);
	}
	public void carLeaves(Car car) {
		//System.out.println("Thread: " + Thread.currentThread().getName() + " Car ID: " +car.carId);
		try {
			Slot slot=slots.stream() .filter(m->m.car.equals(car)).findAny().get();
			System.out.println("Car ID: " + car.carId + " has left the carpark.");
			slots.get(slots.indexOf(slot)).checkout();
		} catch (NullPointerException e)	{
			e.printStackTrace();
		}
		
	}
	public void payParkingFee (Car car)	{
		System.out.println("Car ID " + car.carId + " stayed for " + 
				(clock.getTime() - car.timeEntered) + ".");
		System.out.println("Total payment: $" + (clock.time-car.timeEntered));
	}
}
