package Carpark;

public class Timer implements Runnable {
	boolean stop = false;
	
	Monitor monitor;
	Clock clock;
	
	final int TIME_UNIT=5;
	
	public Timer(Monitor monitor) {
		this.monitor=monitor;
		this.clock=monitor.carpark.clock;
	}
	@Override
    public void run() {
		while(!stop) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        clock.incrementTime();
	        if(clock.time%TIME_UNIT==0) {
	        	System.out.println(clock.printTime());
				if(clock.time!=0)
					monitor.checkArrival();
			}

		}
    	
    }
}
